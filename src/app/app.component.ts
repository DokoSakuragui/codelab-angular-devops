import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

/* JavasCript Normal
export class AppComponent {

  users = ['Luis' , 'Iván' , 'Carmen' , 'Sofia' , 'Santiago'];
  activated = true;
}
*/
/* TypeScript
export class AppComponent {

  users = ['Luis' , 'Iván' , 'Carmen' , 'Sofia' , 'Santiago'];

  activated: boolean ;
  name: string;
  age: number;
  address: {
    street: string,
    city: string
  };
  hobbies: string[];

  constructor(){
    this.name = 'Luis Antonio Aguilar';
    this.age = 28;
    this.address = {street: 'Calle Cecilia 447 ', city: 'Nezahualcoyotl'};
    this.hobbies = ['Leer' , 'Cantar' , 'Brincar' , 'Sonreir' , 'llorar'];
  }

  export class AppComponent {

    users: string[] = ['Luis' , 'Iván' , 'Carmen' , 'Sofia' , 'Santiago'];

    sayHello(){
      alert('Hola!');
    }
    */

  export class AppComponent {

    name: string = 'Luis';
    age: number = 28;

    users: string[] = ['Luis' , 'Iván' , 'Carmen' , 'Sofia' , 'Santiago'];

    addUser (newUser) {
      console.log(newUser.value);
      this.users.push(newUser.value);
      newUser.value = ' ';
      newUser.focus();
      return false;
    }

    deleteUser (user) {
      for (let i = 0; i < this.users.length; i++ ){
        if (user === this.users[i]){
          this.users.splice(i, 1);
        }
      }
    }
  }
